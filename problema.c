#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_BUFFER_CMD 200
#define MAX_NUM_ARGS 50
#define READ_END 0
#define WRITE_END 1

char buffer_cmd[MAX_BUFFER_CMD] = {0};
char *args_p[MAX_NUM_ARGS];

void args_cmd();

int main() {
  
  while(1) {
    printf("MiConsola$ ");

    fgets(buffer_cmd, MAX_BUFFER_CMD, stdin);
    if(strlen(buffer_cmd) > 0 && buffer_cmd[strlen(buffer_cmd) - 1] == '\n'){
      buffer_cmd[strlen(buffer_cmd) - 1] = '\0';
    }

    /*Obtener los parametros de comando.*/
    args_cmd();

    if(!strcmp(buffer_cmd, "exit")) {
      break;
    }

    if(strcmp(buffer_cmd, "")) {
      pid_t pid;
      int fd[2];

      if(pipe(fd) == -1) {
        printf("Error");
      }

      pid = fork();
      
      if(pid < 0) {
        printf("Error\n");
      }
      else if(pid == 0) {
        /*Hijo*/
        close(fd[READ_END]);
        
        //Copia la ejecucion del cmd a stdout.
        dup2(fd[WRITE_END], 1);
        close(fd[WRITE_END]);

        execvp(args_p[0], args_p);
        return -1;
      } 
      else {
        /*Padre*/
        close(fd[WRITE_END]);
        int status;
        waitpid(pid, &status, 0);

        if(WEXITSTATUS(status) == 0) {
          char buffer_pipe[2000];
          int n;
          //Lea lo que el hijo escribio.
          while((n = read(fd[READ_END], buffer_pipe, 2000)) != 0) {
          printf("%s", buffer_pipe);
          memset(buffer_pipe, 0, 2000);
          }
        }else {
          printf("Error: No se reconoce ese comando. \n");
        }
        
        close(fd[READ_END]);
      }
    }
    memset(buffer_cmd, 0, MAX_BUFFER_CMD);
    
  }

  printf("Termina Programa\n");
  return 0;
	
}

void args_cmd() {
  char *ptr;
  int i = 0;
  ptr = strtok(buffer_cmd, " ");
  
  while(ptr != NULL) {
    args_p[i] = ptr;
    i++;
    ptr = strtok(NULL, " ");
  }
}